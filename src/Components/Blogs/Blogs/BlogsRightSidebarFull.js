import React from "react";
import SideBar from "../Common/SideBar";
import BreadCrumb from "../../../GlobalComponents/BreadCrumb";
import Pagination from "../../../GlobalComponents/Pagination";
import BlogsListFull from "../Common/BlogsListFull";
import {blogComments} from "../../../globalUtility/commonHelper";

class BlogsRightSidebarFull extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            blogList: []
        }
    }

    componentWillMount() {
        const data = blogComments()
        this.setState({ data })
    }

    onChangePage = (pageOfItems) => {
        this.setState({
            blogList: pageOfItems
        });
    }

    handelCategories = (item) =>{
        const data = blogComments()
        let category = item.toString()
        const getData = data.blogList.filter(p => p.category === category)
        this.setState({
            data:{
                blogList: getData || []
            }
        })
    }

    render() {
        const {blogList, data} = this.state
        return(
            <>
                {/* breadcrumb section start */}
                    <BreadCrumb heading="Blogs" subHeading="Blogs Right Sidebar Full"/>
                {/* breadcrumb section end */}
                <section className="blog-page padding-80px-tb blog-detailsstyle1">
                    <div className="container-fluid">
                        <div className="row flex-reverse">
                            {/* right sidebar starts */}
                            <SideBar handelCategories={this.handelCategories}/>
                            {/* right sidebar ends */}
                            <div className="col-xl-9 col-lg-9 col-md-8">
                                {/* blogs starts */}
                                <BlogsListFull blogList={blogList}/>
                                {/* blogs ends */}
                                <Pagination
                                    pageSize={6}
                                    sortType=""
                                    items={(data && data.blogList) || []}
                                    onChangePage={this.onChangePage} />
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default BlogsRightSidebarFull
