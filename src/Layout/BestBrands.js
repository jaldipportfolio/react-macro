import React from "react";
import imageUtil from "../globalUtility/imageHelper";

class BestBrands extends React.Component {
    render() {
        const images = imageUtil.images
        return(
            <>
                {/* best brands section starts */}
                <section className="padding-80px-tb bestbrands border-bottom-medium-dark">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8  center-col margin-bottom-40px lg-margin-bottom-30px md-margin-bottom-25px sm-margin-bottom-20px text-center">
                                {/* best brands heading starts */}
                                <h3 className="font-weight-800 main-font text-transform lg-title-medium md-title-medium sm-title-mdmore">Our Duo Best Brands About Fashion</h3>
                                <span className="md-text-large main-font font-weight-600 sm-text-large">We Have The Best Brands Available For You</span>
                                {/* best brands heading ends */}
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        {/* brand icons starts */}
                        <div className="row">
                            <div className="col-xl-2 col-lg-2 col-md-4 col-6">
                                <div className="brand-icon text-center">
                                    <img className="img-fluid" src={images.bestBrand1} alt="brand-icon" />
                                </div>
                            </div>
                            <div className="col-xl-2 col-lg-2 col-md-4 col-6">
                                <div className="brand-icon text-center">
                                    <img className="img-fluid" src={images.bestBrand2} alt="brand-icon" />
                                </div>
                            </div>
                            <div className="col-xl-2 col-lg-2 col-md-4 col-6 sm-margin-top-15px">
                                <div className="brand-icon text-center">
                                    <img className="img-fluid" src={images.bestBrand3} alt="brand-icon" />
                                </div>
                            </div>
                            <div className="col-xl-2 col-lg-2 col-md-4 col-6 md-margin-top-15px">
                                <div className="brand-icon text-center">
                                    <img className="img-fluid" src={images.bestBrand4} alt="brand-icon" />
                                </div>
                            </div>
                            <div className="col-xl-2 col-lg-2 col-md-4 col-6 md-margin-top-15px">
                                <div className="brand-icon text-center">
                                    <img className="img-fluid" src={images.bestBrand5} alt="brand-icon" />
                                </div>
                            </div>
                            <div className="col-xl-2 col-lg-2 col-md-4 col-6 md-margin-top-15px">
                                <div className="brand-icon text-center">
                                    <img className="img-fluid" src={images.bestBrand6} alt="brand-icon" />
                                </div>
                            </div>
                        </div>
                        {/* brand icons ends */}
                    </div>
                </section>
                {/* best brands section starts */}
            </>
        )
    }
}

export default BestBrands;
