export const SET_PRODUCT_LIST = 'SET_PRODUCT_LIST';
export const ADD_TO_CART_PRODUCT = 'ADD_TO_CART_PRODUCT';
export const END_ADD_TO_CART = 'END_ADD_TO_CART';
export const ADD_TO_COMPARE_PRODUCT = 'ADD_TO_COMPARE_PRODUCT';
export const REMOVE_TO_COMPARE_PRODUCT = 'REMOVE_TO_COMPARE_PRODUCT';
export const END_ADD_TO_COMPARE = 'END_ADD_TO_COMPARE';
export const ADD_TO_WISH_LIST_PRODUCT = 'ADD_TO_WISH_LIST_PRODUCT';
export const END_ADD_TO_WISH_LIST = 'END_ADD_TO_WISH_LIST';
export const REMOVE_TO_WISH_LIST_PRODUCT = 'REMOVE_TO_WISH_LIST_PRODUCT';
