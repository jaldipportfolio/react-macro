import {toast} from "react-toastify";

export const setWishList = (data) => {
    const allWishList = JSON.parse(localStorage.getItem('wishList') ) || []
    const isExists = allWishList && allWishList.length && allWishList.find(d => d.id === data.id)
    if (isExists && Object.keys(isExists).length) {
        const idx = allWishList.findIndex(f => f.id === isExists.id)
        allWishList[idx].count = allWishList[idx].count + 1
    } else {
        allWishList.push({...data, count: 1})
    }
    toast.success(`Item has been added to wishList`, {
        position: toast.POSITION.TOP_RIGHT
    });
    localStorage.setItem('wishList', JSON.stringify(allWishList))
    return allWishList
}

export const setCart = (data) => {
    const allCart = JSON.parse(localStorage.getItem('cart')) || []
    const isExists = allCart && allCart.length && allCart.find(d => d.id === data.id)
    if (isExists && Object.keys(isExists).length) {
        const idx = allCart.findIndex(f => f.id === isExists.id)
        allCart[idx].count = allCart[idx].count + (data.count ? data.count : 1)
    } else {
        allCart.push({...data, count: data.count ? data.count : 1})
    }
    toast.success(`Item has been added to cart`, {
        position: toast.POSITION.TOP_RIGHT
    });
    localStorage.setItem('cart', JSON.stringify(allCart))
    return allCart
}

export const prepareCompareList = (item) => {
    let compare = localStorage.getItem("compareList")
    let compareListData = JSON.parse(compare) || []
    const isExist = compareListData.find(p => p.id === item.id)
    if(isExist){
        toast.warn(`This item already added to compare`, {
            position: toast.POSITION.TOP_RIGHT
        });
    }else {
        compareListData.push(item)
        if((compareListData && compareListData.length) > 3){
            compareListData.shift();
        }
        toast.success(`Item has been added to compare`, {
            position: toast.POSITION.TOP_RIGHT
        });
        localStorage.setItem("compareList" , JSON.stringify(compareListData))
        return compareListData
    }
}
